#include "../partials/option.mligo"
#include "../partials/token_storage.mligo"

type create_token_params = option_constructor_params

type update_token_params = token_meta

type token_endpoint = Create of create_token_params | Update of update_token_params

type token_result = operation list * token_storage

let token_main (endpoint, storage: token_endpoint * token_storage): token_result =
  match endpoint with
    | Create params -> let token_meta = create_option params storage.token_id in
      let storage = add_token Tezos.sender token_meta storage in
      ([]: operation list), storage
    | Update params -> let storage = update_token params storage in
      ([]: operation list), storage