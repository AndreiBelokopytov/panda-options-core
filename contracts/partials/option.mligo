type option_id = nat

type option_state =
  | Inactive
  | Active
  | Exercised
  | Expired

type option_kind =
  | Call
  | Put

type option =
[@layout:comb]
{
  id: option_id;
  strike: nat;
  amount: nat;
  expiration: timestamp;
  kind: option_kind;
  state: option_state;
}

type option_constructor_params =
[@layout:comb]
{
  strike: nat;
  amount: nat;
  expiration: timestamp;
  kind: option_kind;
}

let get_next_option_id (current_id: option_id): option_id = current_id + 1n

let create_option (params: option_constructor_params) (current_id: option_id): option =
  let next_id = get_next_option_id current_id in
  {
    id = next_id;
    strike = params.strike;
    amount = params.amount;
    expiration = params.expiration;
    kind = params.kind;
    state = Active ()
  }
