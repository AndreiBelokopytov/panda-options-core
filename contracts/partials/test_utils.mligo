let validate_error_message (error: test_exec_error) (error_message: string): unit =
  match error with
    | Rejected (code, _) -> let expected = Test.eval error_message in
      assert (Test.michelson_equal code expected)
    | Other -> failwith "Unknown error"