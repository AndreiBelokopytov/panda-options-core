#include "option.mligo"

type token_id = option_id

type token_meta = option

type token_info = (token_id, token_meta) big_map

type token_balances = (address, token_id list) big_map

type token_storage =
{
  token_id: token_id;
  token_info: token_info;
  balances: token_balances;
}

let no_token_error = "The token does not exist"

let find_token (token_id: token_id) (token_info: token_info): token_meta =
  let token = Big_map.find_opt token_id token_info in
  match token with
    | Some v -> v
    | None -> (failwith no_token_error: token_meta)

let add_token (owner: address) (meta: token_meta) (storage: token_storage): token_storage =
  let token_info = Big_map.add meta.id meta storage.token_info in
  let current_balance = Big_map.find_opt owner storage.balances in
  let balances = match current_balance with
    | Some owners_balance ->
      Big_map.update owner (Some (meta.id :: owners_balance)) storage.balances
    | None -> Big_map.add owner (meta.id :: []) storage.balances in
  { storage with token_id = meta.id; token_info = token_info; balances = balances }

let update_token (meta: token_meta) (storage: token_storage): token_storage =
  let token_info = Big_map.update meta.id (Some (meta)) storage.token_info in
  { storage with token_info = token_info }
