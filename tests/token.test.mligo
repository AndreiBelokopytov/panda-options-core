#include "../contracts/partials/token_storage.mligo"
#include "../contracts/partials/option.mligo"
#include "../contracts/main/token.mligo"

let initial_storage: token_storage =
{
  token_id = 1000n;
  token_info = (Big_map.empty: token_info);
  balances = (Big_map.empty: token_balances);
}

let test_account = Test.nth_bootstrap_account 1

let (token_ta, _code, _size) = Test.originate token_main initial_storage 0tez

let token_contract = Test.to_contract token_ta

let test_create_token =
  let result = Test.transfer_to_contract token_contract (Create ({
    strike = 10000n;
    amount = 100n;
    kind = Call ();
    expiration = ("2000-01-01t10:10:10Z": timestamp)
  }: create_token_params)) 0tez in
  match result with
    | Success _ -> let storage = Test.get_storage token_ta in
      let () = assert (storage.token_id > initial_storage.token_id) in
      let () = assert (Big_map.mem storage.token_id storage.token_info) in
      assert (Big_map.mem test_account storage.balances)
    | Fail exception -> failwith "Expected the correct contract execution"

let test_update_token =
  let storage = Test.get_storage token_ta in
  let token = find_token storage.token_id storage.token_info in
  let meta = { token with state = Expired() } in
  let result = Test.transfer_to_contract token_contract (Update (meta)) 0tez in
  match result with
    | Success _ -> let storage = Test.get_storage token_ta in
      let modified_token = find_token storage.token_id storage.token_info in
      assert (modified_token.state = Expired())
    | Fail exception -> failwith "Expected the correct contract execution"